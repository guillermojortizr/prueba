class StockService {
  static stocks = [
    {
      id: 856,
      stock: 1
    },
    {
      id: 365,
      stock: 4
    },
    {
      id: 432,
      stock: 6
    },
    {
      id: 937,
      stock: 2
    }
  ];

  static getAll() {
    return this.stocks;
  }

  static findOne(id) {
    return this.stocks.find(element => element.id == id);
  }

  static alter(id, quantity) {
    const index = [...this.stocks].findIndex(element => element.id === id);
    this.stocks[index].stock += quantity;
  }

  static check(item) {
    if (item.quantity > StockService.findOne(item.id).stock) {
      item.required = item.quantity;
      item.available = StockService.findOne(item.id).stock;
      return item;
    }
  }
}

export { StockService };
