class ProductService {
  static products = [
    {
      id: 856,
      price: 229990,
      name: 'SAMSUNG GALAXY A51 128GB'
    },
    {
      id: 365,
      price: 869990,
      name: 'HP PAVILION GAMING 15-DK1022LA'
    },
    {
      id: 432,
      price: 149990,
      name: 'ASPIRADORA ROBOT'
    },
    {
    id: 937,
    price: 269990,
    name: "LED AOC 50\" 50U6295 4K"
    }
  ];

  static getAll() {
    return this.products;
  }

  static findOne(id) {
    const product = this.products.find(element => element.id == id);
    if (!product) {
      throw new Error('Product not found');
    }
    return product;
  }
}
export { ProductService };
