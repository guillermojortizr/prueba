class CalculationService {
  static calculateTax(amount, percentage = 19) {
    return Math.round(amount * (percentage ? 1 + percentage / 100 : 1));
  }

  static calculateDiscount(amount, percentage) {
    return Math.round(amount * (percentage ? percentage / 100 : 1));
  }
}
export { CalculationService };
