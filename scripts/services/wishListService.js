class WishListService {
  constructor(wishList = []) {
    this.wishList = wishList;
  }

  getAll() {
    return this.wishList;
  }

  findOne(id) {
    return this.wishList.find(element => element.id == id);
  }
}
export { wishListService };
