import { ProductService } from '../services/productService.js';
import { StockService } from '../services/stockService.js';
import { CalculationService } from '../services/calculationService.js';

class Car {
  TAX = 19;
  DISCOUNT = 3;

  constructor(items = []) {
    this.items = items;
  }

  addCar(item = null) {
    const product = ProductService.findOne(item.id);
    item.product = product;
    // si existe el previamente el elemento solo suma la cantidad
    if (item && this.items.some(element => element.id == item.id)) {
      const index = this.items.findIndex(element => element.id == item.id);
      this.items[index].quantity += item.quantity;

      // si no existe el elemento en el arreglo y el nuevo elemento es valido
      // se añade el nuevo elemento al arreglo, evitanto que estos se repitan
    } else if (item && item.id && item.quantity > 0) {
      this.items = [...new Set(this.items), item];
    }
    return this.items;
  }

  static checkout(items) {
    const produtcsToPay = [];
    const productsOutOfStock = [];

    items.forEach(item => {
      // 3.2 cantidad de stock (inventario disponible)
      if (StockService.findOne(item.id).stock < item.quantity) {
        productsOutOfStock.push(item);
      } else {
        // 3.3 descuento de 3% en los productos con precios menores a 150000
        if (item.product.price < 150000) {
          item.salePrice = CalculationService.calculateDiscount(
            item.product.price,
            this.DISCOUNT
          );
        } else {
          item.salePrice = item.product.price;
        }

        // 3.1 19% de impuestos
        item.priceWithTaxes = CalculationService.calculateTax(
          item.salePrice,
          this.TAX
        );

        item.subTotal = item.priceWithTaxes * item.quantity;

        produtcsToPay.push(item);
      }
    });
    return { productsOutOfStock, produtcsToPay };
  }
}

export { Car };
