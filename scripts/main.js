import { Car } from './classes/car.js';

const main = () => {
  /*
  1.- El usuario tiene una lista de deseos previamente cargada y decide transferirlo al carrito/bolsa e iniciar el proceso de pago, para ello la tienda debe instanciar una clase "Car" para crear su carro con los productos del "wishList" con las siguientes cantidades:

	1.1 id	-> 365; cantidad-> 4
  1.2 id	-> 856; cantidad-> 5 
  1.3 id	-> 432; cantidad-> 7 
  */

  const items = [
    { id: 365, quantity: 4 },
    { id: 856, quantity: 5 },
    { id: 432, quantity: 7 }
  ];
  const car = new Car();
  items.forEach(item => item && car.addCar(item));
  /*
  2.- Justo antes de ir apagar el usuario observa otro producto "sugerido" (937) y decide sumarlo al carro mendiante un método llamado "addCar"
  */
  car.addCar({ id: 937, quantity: 1 });
  /*
  3.- simular el pago, la tienda debera usar un servicio de "checkout" (método) para calcular el precio total de todos los productos en el carro considerando:

	3.1 19% de impuestos
  3.2 cantidad de stock (inventario disponible)
  3.3 descuento de 3% en los productos con precios menores a 150000
  */
  const result = Car.checkout(car.items);
  /*
  4.- Como output solo se debe logear en estricto orden:

	4.1 la creación del carro (productos y cantidades ordenado por precio)
  4.2 los productos que no cumple con las cantidades de stock disponible
  4.3 cantidad total a pagar por el cliente acompañado con el siguiente mensaje: 
  "Gracias por comprar y ser parte de la experiencia, el monto a pagar es: "
  */
  result.produtcsToPay.sort(
    (itemA, itemB) => itemA.salePrice - itemB.salePrice
  );

  console.log('Productos añadidos al carro de compras:');
  result.produtcsToPay.forEach(item => {
    console.log(item);
  });
  console.log('Productos sin stock:');
  result.productsOutOfStock.forEach(item => {
    console.log(item);
  });
  console.log(
    'Gracias por comprar y ser parte de la experiencia, el monto a pagar es: ',
    result.produtcsToPay.reduce((x, item) => {
      return (x += item.subTotal);
    }, 0)
  );
};

main();
